/**
 * Created by rebzden on 18/10/16.
 */
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var jwt = require('jsonwebtoken');
var jwtConfig = require('./../config/jwtConfig.json');
var validator = require('validator');

var userSchema = new mongoose.Schema({
    username: {
        type: String,
        unique: true,
        required: true
    },
    active: {
        type: Boolean,
        required: true,
        default: false,
        unique: false
    },
    email: {
        type: String,
        required: true,
        unique: true,
        validate: {
            validator: function (value, cb) {
                cb(validator.isEmail(value));
            },
            message: '{VALUE} is not a valid email!'
        },
    },
    password_hash: {
        type: String,
        required: true,
        unique: false
    },
    language: {
        type: mongoose.Schema.Types.ObjectId,
        unique: false,
        required: false
    },
    subscriptions: [
        {
            type: mongoose.Schema.Types.ObjectId,
            unique: true,
            required: false,
            ref: 'Blog'
        }
    ],
    favourite_recipes: [
        {
            type: mongoose.Schema.Types.ObjectId,
            unique: true,
            required: false,
            ref: 'Recipe'
        }
    ],
    preffered_kitchen: [
        {
            type: mongoose.Schema.Types.ObjectId,
            unique: true,
            required: false,
            ref: 'KitchenType'
        }
    ],
    blogs: [
        {
            type: mongoose.Schema.Types.ObjectId,
            unique: true,
            required: false,
            ref: 'Blog'
        }
    ],
    country: {
        type: mongoose.Schema.Types.ObjectId,
        unique: false,
        required: false,
        ref: 'Country'
    },
    role: {
        type: Number,
        required:true,
        default: 1
    },
    created_at: {type: Date, default: Date.now},
    updated_at: {type: Date, default: Date.now},
});
userSchema.methods.setPassword = function (password) {
    this.password_hash = bcrypt.hashSync(password);
};
userSchema.methods.validatePassword = function (password) {
    return bcrypt.compareSync(password, this.password_hash);
};
userSchema.methods.isAdmin = function () {
    return this.role == 99;
};
userSchema.statics.findByEmail = function (email, cb) {
    return this.find({email: email}).active().exec(cb);
};
userSchema.statics.findByUsername = function (username, cb) {
    return this.find({username: username}).active().exec(cb);
};
userSchema.query.active = function () {
    return this.find({active: true});
};
userSchema.query.admin = function () {
    return this.find({role: 99});
};
userSchema.methods.generateJwt = function () {
    var expiry = new Date();
    expiry.setDate(expiry.getDate() + 7);

    return jwt.sign({
        _id: this._id,
        email: this.email,
        username: this.username,
        exp: parseInt(expiry.getTime() / 1000),
    }, jwtConfig.secret);
};
userSchema.path('email').validate(function (value, done) {
    this.model('User').count({email: value}, function (err, count) {
        if (err) {
            return done(err);
        }
        // If `count` is greater than zero, "invalidate"
        done(!count);
    });
}, 'Email already exists');
userSchema.path('username').validate(function (value, done) {
    this.model('User').count({username: value}, function (err, count) {
        if (err) {
            return done(err);
        }
        // If `count` is greater than zero, "invalidate"
        done(!count);
    });
}, 'Username already exists');
module.exports = mongoose.model('User', userSchema);