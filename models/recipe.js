/**
 * Created by rebzden on 18/10/16.
 */
var mongoose = require('mongoose');

var recipeSchema = new mongoose.Schema({
    name: {
        type: String,
        unique: false,
        required: false
    },
    description: {
        type: String,
        unique: false,
        required: false
    },
    prep_time: {
        type: Number,
        unique: false,
        required: false
    },
    kcal: {
        type: Number,
        unique: false,
        required: false
    },
    difficulty: {
        type: String,
        unique: false,
        required: false
    },
    blog: {
        type: mongoose.Schema.Types.ObjectId,
        unique: false,
        required: true,
        ref: 'Blog'
    },
    tags: [
        {
            type:  mongoose.Schema.Types.ObjectId,
            unique: true,
            required: false
        }
    ],
    steps: [
        {
            type: mongoose.Schema.Types.ObjectId,
            unique: false,
            required: false,
            ref: 'RecipeStep'

        }
    ],
    ingredients: [{
        ingredient:{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Ingredient'
        },
        quantity: String
    }],
    kitchen_types: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'KitchenType'
        }
    ],
    photos: [
        {
            type: String,
            unique: false,
            required: false
        }
    ],
    country: {
        type: mongoose.Schema.Types.ObjectId,
        unique: false,
        required: false,
        ref: 'Country'
    },
    created_at: {type: Date, default: Date.now},
    updated_at: {type: Date, default: Date.now},
});
module.exports = mongoose.model('Recipe', recipeSchema);
