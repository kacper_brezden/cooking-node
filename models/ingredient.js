/**
 * Created by rebzden on 19/10/16.
 */
var mongoose = require('mongoose');


var ingredientSchema = new mongoose.Schema({
    description: {
        type: String,
        unique: false,
        required: false
    },
    name: {
        type: String,
        unique: true,
        required: true
    },
    tags: [
        {
            type: String,
            unique: true,
            required: false
        }
    ],
    created_at: {type: Date, default: Date.now},
    updated_at: {type: Date, default: Date.now},
});
module.exports = mongoose.model('Ingredient', ingredientSchema);
