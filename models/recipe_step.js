/**
 * Created by rebzden on 19/10/16.
 */
var mongoose = require('mongoose');


var recipeStepSchema = new mongoose.Schema({
    name: {
        type: String,
        unique: false,
        required: true
    },
    description: {
        type: String,
        unique: false,
        required: false
    },
    photo:{
        type:String,
        unique: false,
        required:false
    },
    created_at: {type: Date, default: Date.now},
    updated_at: {type: Date, default: Date.now},
});
module.exports = mongoose.model('RecipeStep', recipeStepSchema);
