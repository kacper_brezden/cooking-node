/**
 * Created by rebzden on 19/10/16.
 */
var mongoose = require('mongoose');


var blogSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String,
        required: false,
        unique: false
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        unique: false,
        required: true,
        ref:'User'
    },
    recipes: [{
        type: mongoose.Schema.Types.ObjectId,
        unique: false,
        required: true,
        ref:'Recipe'
    }],
    category: {
        type: mongoose.Schema.Types.ObjectId,
        unique: false,
        required: false,
        ref: 'BlogCategory'
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    updated_at: {
        type: Date,
        default: Date.now
    },
});
module.exports = mongoose.model('Blog', blogSchema);