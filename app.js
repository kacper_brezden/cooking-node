var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('express-cors');
var mongoose = require('mongoose');
var sanitizer = require('sanitize-html');
mongoose.Promise = global.Promise;

var blogRoutes = require('./controllers/blog');
var userRoutes = require('./controllers/user');
var adminRoutes = require('./controllers/admin');
var recipeRoutes = require('./controllers/recipe');
var fileRoutes = require('./controllers/file');
var kitchenTypeRoutes = require('./controllers/kitchenType');
var ingredientRoutes = require('./controllers/ingredient');
var filterRoutes = require('./controllers/filter');
var app = express();

mongoose.connect('mongodb://chef:Makaron@ds021036.mlab.com:21036/heroku_664p9x8s').then(function () {
    console.log('connection succesfull')
}).catch(function (err) {
    console.error(err)
});
//CORS middleware
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'POST, PUT, PATCH, DELETE, GET, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type,Origin, X-Requested-With, Accept, Authorization, Access-Control-Allow-Origin');
    next();
}
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(allowCrossDomain);
app.use('/blog', blogRoutes);
app.use('/recipe', recipeRoutes);
app.use('/user', userRoutes);
app.use('/file', fileRoutes);
app.use('/ingredient', ingredientRoutes);
app.use('/kitchen-type', kitchenTypeRoutes);
app.use('/filter', filterRoutes);
app.use('/admin', adminRoutes);
// Add headers
app.use(cors({
    "origin": "*",
    "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
}));
// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to User
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;
