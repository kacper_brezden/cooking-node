/**
 * Created by rebzden on 21/10/16.
 */
var jwt = require('express-jwt');

var jwtConfig = require('./../config/jwtConfig.json');


module.exports = jwt(jwtConfig);