/**
 * Created by rebzden on 21/10/16.
 */



function Controller() {
    var express = require('express');
    var router = express.Router();

    var multer = require('multer');
    var upload = multer({dest: 'uploads/'});
    var auth = require('./jwt-auth');

    this.safeGet = function (path, closure) {
        return router.get(path, closure);
    };

    this.safePost = function (path, closure) {
        return router.post(path, closure);
    };

    this.protectedPost = function (path, closure) {
        return router.post(path, auth, closure);
    };

    this.protectedGet = function (path, closure) {
        return router.get(path, auth, closure);
    };
    this.protectedDelete = function(path,closure){
        return router.delete(path, auth, closure);
    };
    this.getRouter = function () {
        return router;
    };
    this.protectedFilePost = function (path, closure) {
        return router.post(path, auth, closure);
    };
    this.safeFilePut = function (path, closure) {
        return router.put(path, upload.single('file'), closure);
    }
};

module.exports = Controller;


