/**
 * Created by rebzden on 20/10/16.
 */
var baseController = new (require('./../components/controller'))();

var mongoose = require('mongoose');
var User = require('./../models/user');
mongoose.model('User');
/**
 * Search through users
 */
baseController.protectedGet('/', function (req, res) {
    User.find({}).exec(function (err, users) {
        if (err) {

        } else {
            res.status(200).json(users);
        }
    });

});

/**
 * Add user
 */
baseController.safePost('/', function (req, res) {
    var user = new User();
    user.email = req.body.email;
    user.username = req.body.username;
    var error = user.validateSync(['email', 'username', 'password']);
    user.setPassword(req.body.password);
    if (!error) {
        user.save(function (err) {
            if (err) {
                res.status(400);
                res.json({
                    "error": err
                });
            } else {
                var token;
                token = user.generateJwt();
                res.status(200);
                res.json({
                    "token": token
                });
            }
        });
    } else {
        res.status(400).json({
            "error": error
        });
    }
});

/**
 * Authenticate user
 */
baseController.safePost('/login', function (req, res) {
    var status = 400;
    var response = {error: "You have to enter username and password"};
    if (req.body.username && req.body.password) {
        User.findOne({username: req.body.username}, function (err, user) {
            if (err) {
                status = 500;
                response = {error: "There was error while processing your request. Please try again later."};
            } else if (user && user.validatePassword(req.body.password)) {
                status = 200;
                response = {
                    id: user._id,
                    token: user.generateJwt(),
                    blogs: user.blogs,
                    favourite_recipes: user.favourite_recipes,
                    subscriptions: user.subscriptions
                };
            } else {
                status = 401;
                response = {error: "Given username or password is incorrect!"};
            }
            res.status(status).json(response);
        });
    } else {
        res.status(status).json(response);
    }
});

baseController.protectedGet('/kitchen-type', function (req, res) {
    var status;
    var response;
    User.findOne({"_id": req.payload._id}).exec(function (err, model) {
        if (err) {
            status = 500;
            response = err;
        } else {
            status = 200;
            response = model.preffered_kitchen;
        }
        res.status(status).json(response);
    });
});

baseController.protectedPost('/kitchen-type', function (req, res) {
    var status;
    var response;
    console.log(req.body.kitchen_types);
    User.findOneAndUpdate(req.payload._id, {$set: {preffered_kitchen: req.body.kitchen_types}}).exec(function (err, model) {
        if (err) {
            status = 500;
            response = err;
        } else {
            status = 200;
            response = model.preffered_kitchen;
        }
        res.status(status).json(response);
    });
});

baseController.protectedDelete('/:id',function (req, res) {
    var status = 401;
    var response = {};
    User.findOne({_id:req.payload._id}).admin().exec(function(err,model){
        if(err){
            status = 500;
            response = err;
            res.status(status).json(response);
        }else if(model!=null){
            User.remove({_id:req.params.id},function(err){
                if(err){
                    status = 500;
                    response = err;
                }else{
                    status = 200;
                    response = {success:true};
                    res.status(status).json(response);
                }
            });
        }
    });

});

module.exports = baseController.getRouter();