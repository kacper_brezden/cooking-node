/**
 * Created by rebzden on 07/11/16.
 */


var baseController = new (require('./../components/controller'))();
var path = require('path');

baseController.safeFilePut('/upload', function (req, res) {
    res.status(200).json({"file": req.file.filename});
});

baseController.safeGet('/:id',function(req,res){
    res.sendFile(req.params.id, { root: path.join(__dirname, '../uploads') });
});

module.exports = baseController.getRouter();