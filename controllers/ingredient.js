/**
 * Created by rebzden on 20/10/16.
 */
var baseController = new (require('./../components/controller'))();
var mongoose = require('mongoose');
var Ingredient = require('./../models/ingredient');
mongoose.model('Ingredient');

var User = require('./../models/user');
mongoose.model('User');

baseController.safePost('/', function (req, res) {
    var status = 400;
    var response = {};
    var ingredient = new Ingredient();
    ingredient.name = req.body.name;
    ingredient.description = req.body.description;
    ingredient.tags = req.body.tags;
    ingredient.save(function (err) {
        if (err) {
            response = err;
            status = 400;
        } else {
            response = ingredient;
            status = 200;
        }
        res.status(status).json(response);
    });
});

baseController.safeGet('/:id', function (req, res) {
    var status = 400;
    var response = {};
    Ingredient.find({_id: req.params.id}).populate('users')
        .exec(function (err, ingredient) {
            if (err) {
                status = 500;
                response = {error: err};
            } else {
                status = 200;
                response = ingredient;
            }
            res.status(status).json(response);
        });
});
baseController.safeGet('/', function (req, res) {
    var status = 400;
    var response = {};
    Ingredient.find({}).sort({name: 1})
        .exec(function (err, ingredient) {
            if (err) {
                status = 500;
                response = {error: "There was error while processing your request. Please try again later."};
            } else {
                status = 200;
                response = ingredient;
            }
            res.status(status).json(response);
        });
});
baseController.protectedDelete('/:id', function (req, res) {
    var status = 401;
    var response = {};
    User.findOne({_id: req.payload._id}).admin().exec(function (err, model) {
        if (err) {
            status = 500;
            response = err;
            res.status(status).json(response);
        } else if (model != null) {
            Ingredient.remove({_id: req.params.id}, function (err) {
                if (err) {
                    status = 500;
                    response = err;
                } else {
                    status = 200;
                    response = {success: true};
                    res.status(status).json(response);
                }
            });
        }
    });

});
module.exports = baseController.getRouter();