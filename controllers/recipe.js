/**
 * Created by rebzden on 20/10/16.
 */
var baseController = new (require('./../components/controller'))();

var mongoose = require('mongoose');
var Recipe = require('./../models/recipe');
mongoose.model('Recipe');

var RecipeStep = require('./../models/recipe_step');
mongoose.model('RecipeStep');

var User = require('./../models/user');
mongoose.model('User');

var Ingredient = require('./../models/ingredient');
mongoose.model('Ingredient');

baseController.protectedPost('/:id', function (req, res) {
    var recipe = new Recipe();
    recipe.blog = req.params.id;
    recipe.name = req.body.name;
    recipe.description = req.body.description;
    recipe.kcal = req.body.kcal;
    recipe.difficulty = req.body.difficulty;
    recipe.prep_time = req.body.prep_time;
    recipe.photos = req.body.photos;
    req.body.ingredients.forEach(function (ingredient) {
        recipe.ingredients.push({ingredient: ingredient._id, quantity: ingredient.quantity})
    });
    recipe.kitchen_types = req.body.kitchen_types;
    var steps = [];
    req.body.steps.forEach(function (step) {
        var recipeStep = new RecipeStep();
        steps.push(recipeStep._id);
        recipeStep.name = step.name;
        recipeStep.description = step.description;
        recipeStep.photo = step.photo;
        recipeStep.save(function (err) {
            if (err) {
                res.status(400).json({recipeStep: err});
            }
        });
    });
    recipe.steps = steps;
    recipe.save(function (err) {
        if (err) {
            res.status(400).json(err);
        } else {
            res.status(200).json({success: true});
        }
    });
});
baseController.protectedGet('/favourite', function (req, res) {
    User.findOne({"_id": req.payload._id})
        .populate('favourite_recipes')
        .exec(function (err, model) {
            if (err) {

            } else {
                res.status(200).json(model.favourite_recipes);
            }
        });
});


baseController.protectedGet('/favourite/:id', function (req, res) {
    User.findOne({"_id": req.payload._id}).exec(
        function (err, model) {
            if (err) {

            } else {
                var isInArray = model.favourite_recipes.some(function (friend) {
                    return friend.equals(req.params.id);
                });
                if (isInArray) {
                    User.findOneAndUpdate({_id: model._id}, {$pull: {"favourite_recipes": req.params.id}})
                        .exec(function (err, thing) {
                            res.status(200).json({added: req.params.id});
                        });
                } else {
                    User.findOneAndUpdate({_id: model._id}, {$push: {"favourite_recipes": req.params.id}})
                        .exec(function (err, thing) {
                            res.status(200).json({removed: req.params.id});
                        });
                }
            }
        }
    );
});

baseController.protectedGet('/discover', function (req, res) {
    User.findOne({_id: req.payload._id}).exec(function (err, user) {
        if (err) {

        } else {
            Recipe.find({
                kitchen_types: {"$in": user.preffered_kitchen}
            }).sort({'created_at': 1})
                .limit(20)
                .exec(function (err, recipes) {
                    if (err) {
                        status = 500;
                        response = {error: err};
                    } else {
                        status = 200;
                        response = recipes;
                    }
                    res.status(status).json(response);
                });
        }
    });
});

baseController.safeGet('/:id', function (req, res) {
    var status = 400;
    var response = {};
    Recipe.findOne({_id: req.params.id}).populate('steps kitchen_types ingredients.ingredient')
        .exec(function (err, recipe) {
            if (err) {
                status = 500;
                response = {error: err};
                res.status(status).json(response);
            } else {
                status = 200;
                response = recipe;
                res.status(status).json(response);
            }

        });
});

baseController.safeGet('/', function (req, res) {
    var queryFilters = {};
    if (req.query.name != null && req.query.name != "") {
        queryFilters.name = new RegExp('^.*' + req.query.name + '.*$', "i");
    }
    if (req.query.kitchen_types != null) {
        queryFilters.kitchen_types = {$in: req.query.kitchen_types};
    }
    if (req.query.ingredients != null) {
        queryFilters["ingredients.ingredient"] = {$in: req.query.ingredients};
    }
    Recipe.find(
        queryFilters
    ).sort({'created_at': -1}).limit(20).exec(function (err, recipes) {
        if (err) {
            status = 404;
            response = {error: "No recipes found :("};
        } else {
            status = 200;
            response = recipes;
        }
        res.status(status).json(response);
    });
});

baseController.protectedDelete('/:id', function (req, res) {
    var status = 401;
    var response = {};
    User.findOne({_id: req.payload._id}).admin().exec(function (err, model) {
        if (err) {
            status = 500;
            response = err;
            res.status(status).json(response);
        } else if (model != null) {
            Recipe.remove({_id: req.params.id}, function (err) {
                if (err) {
                    status = 500;
                    response = err;
                } else {
                    status = 200;
                    response = {success: true};
                    res.status(status).json(response);
                }
            });
        }
    });

});

baseController.safeGet('/blog/:id', function (req, res) {
    var status = 400;
    var response = {};
    Recipe.find({blog: req.params.id})
        .exec(function (err, recipes) {
            if (err) {
                status = 404;
                response = {error: "Blog with given id doesn't exist"};
            } else {
                status = 200;
                response = recipes;
            }
            res.status(status).json(response);
        });
});

module.exports = baseController.getRouter();