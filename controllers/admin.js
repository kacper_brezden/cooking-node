/**
 * Created by rebzden on 06/12/16.
 */
var baseController = new (require('./../components/controller'))();

var mongoose = require('mongoose');
var User = require('./../models/user');
mongoose.model('User');

/**
 * Authenticate user
 */
baseController.safePost('/login', function (req, res) {
    var status = 400;
    var response = {error: "You have to enter username and password"};
    if (req.body.username && req.body.password) {
        User.findOne({username: req.body.username}, function (err, user) {
            if (err) {
                status = 500;
                response = {error: "There was error while processing your request. Please try again later."};
            } else if (user && user.validatePassword(req.body.password) && user.isAdmin()) {
                status = 200;
                response = {
                    id: user._id,
                    token: user.generateJwt()
                };
            } else {
                status = 401;
                response = {error: "Given username or password is incorrect!"};
            }
            res.status(status).json(response);
        });
    } else {
        res.status(status).json(response);
    }
});

module.exports = baseController.getRouter();