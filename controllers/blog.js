/**
 * Created by rebzden on 20/10/16.
 */
var baseController = new (require('./../components/controller'))();
var mongoose = require('mongoose');
var Blog = require('./../models/blog');
mongoose.model('Blog');
var User = require('./../models/user');
mongoose.model('User');

baseController.protectedPost('/', function (req, res) {
    var status;
    var response;
    var blog = new Blog();
    blog.name = req.body.name;
    blog.description = req.body.description;
    blog.user = req.payload._id;
    var error = blog.validateSync(['name', 'description', 'user']);
    if (!error) {
        blog.save(function (err) {
            if (err) {
                status = 400;
                response = err;
                res.status(status).json(response);
            } else {
                User.findOneAndUpdate(req.payload._id, {$push: {"blogs": blog._id}})
                    .exec(function (err, user) {
                        if (err) {
                            status = 400;
                            response = err;
                        } else {
                            status = 200;
                            response = {
                                "success": true
                            };
                        }
                        res.status(status).json(response);
                    });
            }
        });
    } else {
        res.status(400).json({
            "error": error
        });
    }
});
baseController.protectedPost('/:id', function (req, res) {
    var response = {};
    var status = 404;
    User.findOne({
        _id: req.payload._id,
        blogs: {$elemMatch: {$in: [req.params.id]}}
    }).exec(function (err, user) {
        console.log(err, user);
        if (err) {
            response = err;
            status = 500;
        } else {
            if (user) {
                Blog.findOneAndUpdate({_id: req.params.id}, {
                    $set: {
                        name: req.body.name,
                        description: req.body.description
                    }
                }).exec(function (err, model) {
                    console.log(err, model);
                    if (err) {
                        response = err;
                        status = 500;
                    } else {
                        status = 200;
                        response = {success: true};
                    }
                    res.status(status).json(response);
                });
            }
        }

    });
});
baseController.safeGet('/:id', function (req, res) {
    Blog.findOne({_id: req.params.id}).populate('users')
        .exec(function (err, blog) {
            if (err) {
                status = 404;
                response = {error: "Blog with given id doesn't exist"};
            } else {
                status = 200;
                response = blog;
            }
            res.status(status).json(response);
        });
});

baseController.safeGet('/user/:id', function (req, res) {
    Blog.find({user: req.params.id})
        .exec(function (err, blog) {
            if (err) {
                status = 404;
                response = {error: "User with given id has no blogs registered"};
            } else {
                status = 200;
                response = blog;
            }
            res.status(status).json(response);
        });
});

baseController.safeGet('/', function (req, res) {
    Blog.find({}).populate('users', 'email')
        .exec(function (err, blog) {
            if (err) {
                status = 500;
                response = {error: "There was error while processing your request. Please try again later."};
            } else {
                status = 200;
                response = blog;
            }
            res.status(status).json(response);
        });
});

baseController.protectedDelete('/:id',function (req, res) {
    var status = 401;
    var response = {};
    User.findOne({_id:req.payload._id}).admin().exec(function(err,model){
        if(err){
            status = 500;
            response = err;
            res.status(status).json(response);
        }else if(model!=null){
            Blog.remove({_id:req.params.id},function(err){
                if(err){
                    status = 500;
                    response = err;
                }else{
                    status = 200;
                    response = {success:true};
                    res.status(status).json(response);
                }
            });
        }
    });

});

module.exports = baseController.getRouter();