/**
 * Created by rebzden on 04/12/16.
 */
var baseController = new (require('./../components/controller'))();

var mongoose = require('mongoose');

var KitchenType = require('./../models/kitchen_type');
mongoose.model('KitchenType');

var Ingredient = require('./../models/ingredient');
mongoose.model('Ingredient');

baseController.safeGet('/', function (req, res) {
    var status = 200;
    var response = [];
    KitchenType.find().exec(function (err, kitchenTypes) {
        if (err) {
            status = 400;
            response = err;
        } else {
            Ingredient.find().exec(function (err, ingredients) {
                if (err) {
                    status = 400;
                    response = err;
                } else {
                    var kitchenTypesFilters = {
                        id: "kitchen_types",
                        name: "Kitchen types",
                        values: []
                    };
                    for (x in kitchenTypes) {
                        kitchenTypesFilters.values.push({
                            id: kitchenTypes[x]._id,
                            name: kitchenTypes[x].name
                        });
                    }
                    var ingredientsFilters = {
                        id: "ingredients",
                        name: "Ingredients",
                        values: []
                    };
                    for (y in ingredients) {
                        ingredientsFilters.values.push({
                            id: ingredients[y]._id,
                            name: ingredients[y].name
                        });
                    }
                    response.push(kitchenTypesFilters);
                    response.push(ingredientsFilters);
                    res.status(200).json(response);
                }
            });
        }
    });
});

module.exports = baseController.getRouter();