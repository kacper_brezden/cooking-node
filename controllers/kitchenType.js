/**
 * Created by rebzden on 20/10/16.
 */
var baseController = new (require('./../components/controller'))();

var mongoose = require('mongoose');
var KitchenType = require('./../models/kitchen_type');
mongoose.model('KitchenType');

var User = require('./../models/user');
mongoose.model('User');

/**
 * Search through kitchen types
 */
baseController.safeGet('/', function (req, res) {
    var status;
    var response;
    KitchenType.find({}).sort({name: 1})
        .exec(function (err, model) {
            if (err) {
                status = 400;
                response = err;
            } else {
                status = 200;
                response = model;
            }
            res.status(status).json(response);
        });
});

baseController.protectedGet('/user', function (req, res) {
    var status;
    var response = [];
    KitchenType.find({}).sort({name: 1})
        .exec(function (err, kitchen_types) {
            if (err) {
                status = 400;
                response = err;
                res.status(status).json(response);
            } else {
                User.findOne({_id: req.payload._id}).exec(function (err, user) {
                    if (err) {
                        response = err;
                        status = 400;
                    } else {
                        console.log(user.preffered_kitchen);
                        for (var j in kitchen_types) {
                            var added = false;
                            if (user.preffered_kitchen && user.preffered_kitchen.length > 0) {
                                for (i = 0; i < user.preffered_kitchen.length; i++) {
                                    if (String(user.preffered_kitchen[i]).valueOf() == String(kitchen_types[j]._id).valueOf()) {
                                        added = true;
                                    }
                                }
                            }
                            var kt = {added: added};
                            kt.kitchen_type = kitchen_types[j];
                            response.push(kt);
                        }
                        status = 200;
                    }
                    res.status(status).json(response);
                });

            }

        });
});

baseController.safePost('/', function (req, res) {
    var status;
    var response = {};
    var kitchenType = new KitchenType();
    kitchenType.name = req.body.name;
    kitchenType.description = req.body.description;
    kitchenType.tags = req.body.tags;
    kitchenType.ingredients = req.body.ingredients;
    kitchenType.save(function (err, model) {
        if (err) {
            status = 400;
            response = err;
        } else {
            status = 200;
            response = {success: true};
        }
        res.status(status).json(response);
    });
});

baseController.protectedDelete('/:id',function (req, res) {
    var status = 401;
    var response = {};
    User.findOne({_id:req.payload._id}).admin().exec(function(err,model){
        if(err){
            status = 500;
            response = err;
            res.status(status).json(response);
        }else if(model!=null){
            KitchenType.remove({_id:req.params.id},function(err){
                if(err){
                    status = 500;
                    response = err;
                }else{
                    status = 200;
                    response = {success:true};
                    res.status(status).json(response);
                }
            });
        }
    });

});
module.exports = baseController.getRouter();